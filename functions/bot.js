const Twit = require('twit');
const CronJob = require('cron').CronJob;
const fs = require('fs');
const config = require('./config');
const request = require('request');

// Konfigurasi Twitter Bot
const Twitter = new Twit(config);

// URL
// let url = 'https://bot-anjing.firebaseapp.com/kata.json';
let url = 'https://us-central1-bot-anjing.cloudfunctions.net/app/list-kata';

// const kosakata = fs.readFileSync('./lib/kata.json');

// Ambil Kosa Kata
// let kosaKata = request({ url: url }, (err, res, body) => console.log(JSON.parse(body), "Parse JSON") );

// Konversi JSON
// let kataKata = JSON.parse(kosakata);

// Index Pertama dalam array kataKata
let kataLength = 0;


// Fungsi untuk acak urutan kosakata
function acak(kata) {
  return kata[Math.floor(Math.random() * kata.length)];
}


// Fungsi Tweet dengan kata-kata yang sudah dipecah
function tweet() {
  request(url, (err, response, body) => {
    Twitter.post('statuses/update', {
      status: `${acak(body)} anjing`
    }, (error, data, response) => {
      console.log(data.text, 'Dari URL')
    });

  });
  kataLength += 1;
}


// Post tweet per-30 menit
const autoTweet = new CronJob({
  cronTime: '*/1 * * * *',
  onTick: () => {
    tweet();
  },
  start: true,
  timeZone: 'Asia/Jakarta'
});

module.exports = autoTweet;