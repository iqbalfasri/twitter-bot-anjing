const functions = require('firebase-functions');
const express = require('express');

const app = express();

const autoTweet = require('./bot');

const kata = require('./lib/kata.json');

app.get('/', (req, res) => {
  autoTweet.start();
  res.json({message: "Bot Jalan..."});
  // Running autotweet
  console.log('Bot Jalan')
});

app.get('/bot', (req, res) => {
  res.json({message: "Bot running..."});
});

app.get('/list-kata', (req, res) => {
  res.json(kata);
});

exports.app = functions.https.onRequest(app);