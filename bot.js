const Twit = require('twit');
const CronJob = require('cron').CronJob;
const fs = require('fs');
const config = require('./config');
const request = require('request');

// Konfigurasi Twitter Bot
const Twitter = new Twit(config);

// Ambil Kosa Kata
// let kosaKata = fs.readFileSync('./lib/kata.json');
let url = 'https://us-central1-bot-anjing.cloudfunctions.net/app/list-kata';

// Konversi JSON
// let kataKata = JSON.parse(kosaKata);
// Index Pertama dalam array kataKata
let kataLength = 0;

// Fungsi untuk acak urutan kosakata
function acak(kata) {
  return kata[Math.floor(Math.random() * kata.length)];
}

// Fungsi Tweet dengan kata-kata yang sudah dipecah
function tweet() {
  request(url, (err, res, body) => {
    Twitter.post('statuses/update', {
      status: `${acak(JSON.parse(body))} anjing`
    }, (error, data, response) => {
      console.log(JSON.stringify(data))
    });
  })
  kataLength += 1;
}

// Post tweet per-30 menit
const autoTweet = new CronJob({
  cronTime: '*/30 * * * *',
  onTick: () => {
    tweet();
  },
  start: true,
  timeZone: 'Asia/Jakarta'
});
module.exports = autoTweet;